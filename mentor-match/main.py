#!/usr/bin/env python

#  Mentor-match main.py 
#
#    Copyright (C) 2013  Jeff Rzeszotarski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import webapp2
import os.path
import datetime
import dateutil

import webapp2
import jinja2

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import mail

#from google.appengine.api import urlfetch
#from google.appengine.api import memcache

import html5lib
from html5lib import treebuilders, treewalkers, serializer, sanitizer

jinja_environment = jinja2.Environment(autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))



#  --- DATABASE MODELS ---


class Revision(db.Model):

    #recall: unique id exists in the form of .key()
    
    antecedent = db.SelfReferenceProperty()
    
    user_id = db.StringProperty()
    username = db.StringProperty()
    timestamp = db.DateTimeProperty(auto_now_add=True)
    
    title = db.StringProperty()
    description = db.TextProperty()
    why_it_matters = db.TextProperty()
    activity = db.TextProperty()
    authors_text = db.TextProperty()
    references = db.TextProperty()
    
    meta = db.StringProperty()


class Micromethod(db.Model):
    latest_revision = db.ReferenceProperty(Revision)
    timestamp = db.DateTimeProperty()
    discipline = db.CategoryProperty(choices=[
                                "Qualitative",
                                "Visual",
                                "Communication",
                                "Industrial",
                                "Interaction",
                                "Software",
                                "Tinkering",
                                "Business",
                                "Research"
                                ])
    title = db.StringProperty()  #duplicated here for search
    disabled = db.BooleanProperty(default=False)

    num_hits = db.IntegerProperty(default=0)
    num_signups = db.IntegerProperty(default=0)    


class Comment(db.Model):
    
    comment_for_method = db.ReferenceProperty(Micromethod)
    
    user_id = db.StringProperty()
    username = db.StringProperty()
    timestamp = db.DateTimeProperty(auto_now_add=True)
    
    comment = db.TextProperty()
    
    meta = db.StringProperty()


class Assignment(db.Model):

    assigned_method = db.ReferenceProperty(Micromethod)
    
    user_id = db.StringProperty()
    username = db.StringProperty()
    user_email = db.StringProperty()
    timestamp = db.DateTimeProperty(auto_now_add=True)
    
    has_nagged = db.BooleanProperty(default=False)
    resolved = db.BooleanProperty(default=False)


# TODO: Implement memcache layer to reduce db hits



#  --- UTILITIES ---

class ContentSanitizer:
    @staticmethod
    def sanitize(content):
        parser = html5lib.HTMLParser(tokenizer = sanitizer.HTMLSanitizer,
                                     tree = treebuilders.getTreeBuilder("dom"))
        dom = parser.parseFragment(content)
        tree_walker = treewalkers.getTreeWalker("dom")
        tree_stream = tree_walker(dom)
        serial = serializer.HTMLSerializer(omit_optional_tags = False,
                                               quote_attr_values = True)
        output = serial.serialize(tree_stream)
        return u''.join(output)


#  --- URL HANDLERS ---

class HomePage(webapp2.RequestHandler):
    def get(self):
        #get user status
        user = users.get_current_user()
        user_nickname = ""
        if user:
            user_nickname = user.nickname()
            user_nickname = user_nickname.replace("@","[at]")
        
        
        methods = Micromethod.all()
        methods.order("-timestamp")
        methods.filter("disabled =", False)
        
        template_values = {
                "count": methods.count(),
                "latest_methods": [],
                "login_url": users.create_login_url("/"),
                "logout_url": users.create_logout_url("/"),
                "user_logged_in": users.get_current_user() is not None,
                "username": user_nickname
            }
        
        #list some recent methods added
        for method in methods.run(limit=5):
            revision = method.latest_revision
        
            rev_dict = {
                "title": revision.title,
                "type": method.discipline,
                "id": method.key().id(),
                "timestamp": method.timestamp.strftime("%A, %d. %B %Y %I:%M%p") 
                }
            template_values["latest_methods"].append(rev_dict)
        
        
        template = jinja_environment.get_template('home.htm')
        self.response.out.write(template.render(template_values))
        
    
class Browse(webapp2.RequestHandler):
    def get(self):
        #get user status
        user = users.get_current_user()
        user_id = None
        user_nickname = ""
        if user:
            user_id = user.user_id()
            user_nickname = user.nickname()
            user_nickname = user_nickname.replace("@","[at]")
    
    
        methods = Micromethod.all()
        methods.order("-timestamp")
        methods.filter("disabled =", False)
        
        #search parameters
        #   lots of room here to improve search
        discipline = self.request.get("discipline", default_value="All")
        if discipline != "All":
            methods.filter("discipline =", discipline)
            
            
            
        search_text = self.request.get("text")
        if search_text != "":
            pass
            #could use the search API here to latch to a "text" text input and do 
            # a search of Micromethod.title fields and filter the query results
            #"text" is passed as a parameter to the template to fill in prev searches
           
        #offsets for multiple pages
        items_per_page = 20
        page_offset = int(self.request.get("offset", default_value=1)) - 1
    
        template_values = {
                "count": methods.count(),
                "num_pages": (methods.count() / items_per_page) + 1,
                "search_results": [],
                "login_url": users.create_login_url(self.request.uri),
                "logout_url": users.create_logout_url(self.request.uri),
                "user_logged_in": users.get_current_user() is not None,
                "username": user_nickname,
                "discipline": discipline,
                "text": search_text
            }    
                    
        #get assignments for marking completed ones
        assignments = []
        if user is not None:
            a = Assignment.all()
            a.filter("user_id =", user_id)
            assignments = a.run()

        #list methods
        for method in methods.run(limit=items_per_page, offset=items_per_page*page_offset):
            revision = method.latest_revision
            
            #could implement this loop with multiple db fetches with filter = method
            #  don't do this to save db hits at expense of cpu
            #  for scalability as assignments rise, switch to multiple db queries
            assigned = False
            completed = False
            for assignment in assignments:
                if (assignment.assigned_method is method):
                    assigned = True
                    completed = assignment.resolved
                    
            
            rev_dict = {
                "title": revision.title,
                "type": method.discipline,
                "id": method.key().id(),
                "timestamp": method.timestamp.strftime("%A, %d. %B %Y %I:%M%p"),
                "assigned": assigned,
                "completed": completed
                }
                
                
            template_values["search_results"].append(rev_dict)
        
        
        template = jinja_environment.get_template('browse.htm')
        self.response.out.write(template.render(template_values))

class ViewMethod(webapp2.RequestHandler):
    def get(self):
        #get user status
        user = users.get_current_user()
        user_id = None
        user_nickname = ""
        if user:
            user_id = user.user_id()
            user_nickname = user.nickname()
            user_nickname = user_nickname.replace("@","[at]")
            
        #poll the method to look up the revision
        method_id = long(self.request.get("method_id", default_value=-1))
        method = Micromethod.get_by_id(method_id)
        if method is None:
            self.redirect("/browse")
            return
            
        method.num_hits += 1
        method.put()
            
        rev = method.latest_revision
        
        #check to see if signed up
        assigned = False
        completed = False
        assigned_on = ""
        if user is not None:
            a = Assignment.all()
            a.filter("assigned_method =", method)
            a.filter("user_id =", user_id)
            assignment = a.get()
            if assignment is not None:
                assigned = True
                completed = assignment.resolved
                assigned_on = assignment.timestamp.strftime("%A, %d. %B %Y %I:%M%p")
        
        
        template_values = {
                "hits": method.num_hits,
                "login_url": users.create_login_url(self.request.uri),
                "logout_url": users.create_logout_url(self.request.uri),
                "user_logged_in": users.get_current_user() is not None,
                "username": user_nickname,
                "discipline": method.discipline,
                "editor": rev.username,
                "title": rev.title,
                "description": rev.description,
                "matters": rev.why_it_matters,
                "activity": rev.activity,
                "authors": rev.authors_text,
                "references": rev.references,
                "timestamp": rev.timestamp.strftime("%A, %d. %B %Y %I:%M%p"),
                "assigned": assigned,
                "completed": completed,
                "assigned_on": assigned_on,
                "method_id": method_id,
                "comments": []
            }  
            
        #poll for all comments
        comments = Comment.all()
        comments.order("-timestamp")
        comments.filter("comment_for_method =", method)
        for comment in comments:
               d = {"username": comment.username,
                    "timestamp": comment.timestamp.strftime("%A, %d. %B %Y %I:%M%p"),
                    "comment": comment.comment
                   }
               template_values["comments"].append(d)
         
            
        template = jinja_environment.get_template('view.htm')
        self.response.out.write(template.render(template_values))
    

class AddRevision(webapp2.RequestHandler):
    def get(self):
        #get user status
        user = users.get_current_user()
        
        #if user is not logged in, escape to login then resubmit
        if user is None:
            self.redirect(users.create_login_url(self.request.uri))
            return
            
        user_id = user.user_id()
        user_nickname = user.nickname()
        user_nickname = user_nickname.replace("@","[at]")
        
        
        #populate some template fields
        template_values = {
                "login_url": users.create_login_url(self.request.uri),
                "logout_url": users.create_logout_url(self.request.uri),
                "user_logged_in": users.get_current_user() is not None,
                "username": user_nickname,
                "discipline": "Qualitative",
                "editor": "No one",
                "title": "Title",
                "description": "",
                "matters": "",
                "activity": "",
                "authors": "",
                "references": "",
                "timestamp": "never",
                "method_id": -1
            }  

        #create a new method if we're starting a new one
        method = None
        method_id = long(self.request.get("method_id", default_value=-1))
        if method_id != -1:
            method = Micromethod.get_by_id(method_id)
            rev = method.latest_revision
            
            template_values["editor"] = rev.username
            template_values["title"] = rev.title
            template_values["description"] = rev.description
            template_values["matters"] = rev.why_it_matters
            template_values["activity"] = rev.activity
            template_values["authors"] = rev.authors_text
            template_values["references"] = rev.references
            template_values["timestamp"] = rev.timestamp.strftime("%A, %d. %B %Y %I:%M%p")
            template_values["method_id"] = method_id

        
        template = jinja_environment.get_template('add.htm')
        self.response.out.write(template.render(template_values))        
  
            
        
    def post(self):
        #get user status
        user = users.get_current_user()
        
        #if user is not logged in, escape to login then resubmit
        if user is None:
            self.redirect(users.create_login_url(self.request.uri))
            return
        
        user_id = user.user_id()
        user_nickname = user.nickname()
        user_nickname = user_nickname.replace("@","[at]")
        
        
        #create a new method if we're starting a new one
        method = None
        antecedent_rev = None
        method_id = long(self.request.get("method_id", default_value=-1))
        if method_id == -1:
            method = Micromethod()
            antecedent_rev = None
        
        #look up an existing method if we're modifying it
        else:
            method = Micromethod.get_by_id(method_id)
            revs = Revision.all()
            revs.order("-timestamp")
            antecedent_rev = revs.get()
        
        
        #create a new revision entry
        #   note that we must sanitize in case they use malevolent HTML
        title = self.request.get("title")
        if len(title) < 1:
            title = "<BLANK>"
        
        rev = Revision()
        rev.title = ContentSanitizer.sanitize(title)
        rev.description = ContentSanitizer.sanitize(self.request.get("description"))
        rev.why_it_matters = ContentSanitizer.sanitize(self.request.get("matters"))
        rev.activity = ContentSanitizer.sanitize(self.request.get("activity"))
        rev.authors_text = ContentSanitizer.sanitize(self.request.get("authors"))
        rev.references = ContentSanitizer.sanitize(self.request.get("references"))
        
        rev.user_id = user_id
        rev.username = user_nickname
        rev.antecedent = antecedent_rev
        
        rev.put()
        
        #update the method entry
        method.latest_revision = rev
        method.timestamp = datetime.datetime.now()
        method.discipline = self.request.get("discipline")
        method.title = ContentSanitizer.sanitize(title)
        method.put()
        
        method_id = method.key().id()
        
        
        self.redirect("/view?method_id={0}".format(method_id))


class AddComment(webapp2.RequestHandler):
    def get(self):
        self.redirect("/")
        return
        
    def post(self):
        #get user status
        user = users.get_current_user()
        
        #if user is not logged in, escape to login then resubmit
        if user is None:
            self.redirect(users.create_login_url(self.request.uri))
            return
        
        user_id = user.user_id()
        user_nickname = user.nickname()
        user_nickname = user_nickname.replace("@","[at]")
        
        #poll the method to look up the revision
        method_id = long(self.request.get("method_id", default_value=-1))
        method = Micromethod.get_by_id(method_id)
        if method is None:
            self.redirect("/browse")
            return
        
        
        c = Comment()
        c.comment_for_method = method
        c.user_id = user_id
        c.username = user_nickname
        c.comment = ContentSanitizer.sanitize(self.request.get("comment"))
        c.put()
        
        self.redirect("/view?method_id={0}".format(method_id))

class Signup(webapp2.RequestHandler):
    def get(self):
        #get user status
        user = users.get_current_user()
        
        #if user is not logged in, escape to login then resubmit
        if user is None:
            self.redirect(users.create_login_url(self.request.uri))
            return
            
        user_id = user.user_id()
        user_nickname = user.nickname()
        user_email = user.email()
        
        #poll the method to look up the revision
        method_id = long(self.request.get("method_id", default_value=-1))
        method = Micromethod.get_by_id(method_id)
        if method is None:
            self.redirect("/browse")
            return
            
        a = Assignment()
        a.user_id = user_id
        a.username = user_nickname
        a.user_email = user_email
        a.assigned_method = method
        a.put()
        
        self.redirect("/view?method_id={0}".format(method_id))


class PollSignups(webapp2.RequestHandler):
    #this reserved for a cron job that checks for signups that need resolution
    def get(self):
        #look for signups that have 7 days passed by
        cutoff = date.now() + dateutil.relativedelta(days = -7)
    
        a = Assignment.all()
        a.filter("resolved =", False)
        a.filter("timestamp <", cutoff)
        for assignment in a:
            assignment.has_nagged = True
            assignment.resolved = True
            
            #now send them an email reminder
            title = assignment.assigned_method.title
            key = assignment.assigned_method.key().id()
            url = r"http://mentor-match.appspot.com/view?method_id={0}".format(key)
            
            mail.send_mail(sender="Mentor-Match System",
              to=assignment.user_email,
              subject="Reminder: Comment on \"%s\"".format(title),
              body="""Dear {0},\n
                 Please don't forget to comment on the Micromethod you recently signed up for: {1}\n
                 To comment, please visit: {2}""".format(title,assignment.username,url)
              )
              
            assignment.put()
            
class Dashboard(webapp2.RequestHandler):
    def get(self):
        pass
        #it would be nice to have a user dashboard to show comments, method owned, signups etc

#  --- DIRECTORY ---

app = webapp2.WSGIApplication([
    ('/dashboard', Dashboard),
    ('/add', AddRevision),
    ('/comment', AddComment),
    ('/view', ViewMethod),
    ('/browse', Browse),
    ('/signup', Signup),
    ('/pollsignups', PollSignups),
    ('/', HomePage)
    
], debug=True)



